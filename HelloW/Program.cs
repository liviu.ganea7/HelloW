using System;

namespace HelloW
{
    /// <summary>
    ///     Application starting point. Includes Hello World! 
    /// </summary>
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
